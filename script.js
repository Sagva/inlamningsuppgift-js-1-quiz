let allClassmates = [
    {name: 'Amanda Olsson', photo: 'images/amanda-olsson.jpg', gender: 'F'},
    {name: 'August Ronnle', photo: 'images/august-ronnle.jpg', gender: 'M'},
    {name: 'Axel Bjornfot', photo: 'images/axel-bjornfot.jpg', gender: 'M'},
    {name: 'Cedrik Von Heiroth', photo: 'images/cedrik-von-heiroth.jpg', gender: 'M'},
    {name: 'Celil Tat', photo: 'images/celil-tat.jpg', gender: 'M'},
    {name: 'Christina Mannerberg', photo: 'images/christina-mannerberg.jpg', gender: 'F'},
    {name: 'Christopher Lindstrom', photo: 'images/christopher-lindstrom.jpg', gender: 'M'},
    {name: 'Daniel Palmdal', photo: 'images/daniel-palmdal.jpg', gender: 'M'},
    {name: 'Elena Myadzeleva', photo: 'images/elena-myadzeleva.jpg', gender: 'F'},
    {name: 'Elin Stenquist', photo: 'images/elin-stenquist.jpg', gender: 'F'},
    {name: 'Emma Andersson', photo: 'images/emma-andersson.jpg', gender: 'F'},
    {name: 'Frida Stenberg', photo: 'images/frida-stenberg.jpg', gender: 'F'},
    {name: 'Henrik Lood', photo: 'images/henrik-lood.jpg', gender: 'M'},
    {name: 'Hitomi Winberg', photo: 'images/hitomi-winberg.jpg', gender: 'F'},
    {name: 'Isabella Bjelobrk', photo: 'images/isabella-bjelobrk.jpg', gender: 'F'},
    {name: 'Jerry Phuong', photo: 'images/jerry-phuong.jpg', gender: 'M'},
    {name: 'Johan Markstrom', photo: 'images/johan-markstrom.jpg', gender: 'M'},
    {name: 'Johannes Hernehult', photo: 'images/johannes-hernehult.jpg', gender: 'M'},
    {name: 'Johnny Lay', photo: 'images/johnny-lay.jpg', gender: 'M'},
    {name: 'Khaled Hassan', photo: 'images/khaled-hassan.jpg', gender: 'M'},
    {name: 'Kyd Kitchaiya', photo: 'images/kyd-kitchaiya.jpg', gender: 'M'},
    {name: 'Leo Eriksson', photo: 'images/leo-eriksson.jpg', gender: 'M'},
    {name: 'Linda Hultemark', photo: 'images/linda-hultemark.jpg', gender: 'F'},
    {name: 'Lisa Hansson', photo: 'images/lisa-hansson.jpg', gender: 'F'},
    {name: 'Lovisa Nordstrom', photo: 'images/lovisa-nordstrom.jpg', gender: 'F'},
    {name: 'Marko Zdravkovski', photo: 'images/marko-zdravkovski.jpg', gender: 'M'},
    {name: 'Mehmet Yazgan', photo: 'images/mehmet-yazgan.jpg', gender: 'M'},
    {name: 'Mikaela Norrelokke', photo: 'images/mikaela-norrelokke.jpg', gender: 'F'},
    {name: 'Miranda Trang', photo: 'images/miranda-trang.jpg', gender: 'F'},
    {name: 'Mona Khorasani', photo: 'images/mona-khorasani.jpg', gender: 'F'},
    {name: 'Oliver Kellgren', photo: 'images/oliver-kellgren.jpg', gender: 'M'},
    {name: 'Oskar Anderberg', photo: 'images/oskar-anderberg.jpg', gender: 'M'},
    {name: 'Pernilla Lundahl', photo: 'images/pernilla-lundahl.jpg', gender: 'F'},
    {name: 'Pucha Sayerz Olsen', photo: 'images/pucha-sayerz-olsen.jpg', gender: 'F'},
    {name: 'Saga Swahn', photo: 'images/saga-swahn.jpg', gender: 'F'},
    {name: 'Sara Mattisson', photo: 'images/sara-mattisson.jpg', gender: 'F'},
    {name: 'Sebastian Mineur', photo: 'images/sebastian-mineur.jpg', gender: 'M'},
    {name: 'Simon Bergstrand', photo: 'images/simon-bergstrand.jpg', gender: 'M'},
    {name: 'Susanne Eneroth', photo: 'images/susanne-eneroth.jpg', gender: 'F'},
    {name: 'Svitlana Rybakova', photo: 'images/svitlana-rybakova.jpg', gender: 'F'},
    {name: 'Tricia Hartmann', photo: 'images/tricia-hartmann.jpg', gender: 'F'},
    {name: 'Zainab Ahmad', photo: 'images/zainab-ahmad.jpg', gender: 'F'}
];


//function for getting the random number depending on the length of an element
const randomNumber = el => Math.floor(Math.random() * el.length); 

//function for generating random classmates, amount can be different
const getRandomClassmates = (classmatesList, amount) => { 

    //array for storing random classmates, this array returns from function
    let randomClassmates = []; 

    //keep adding clasmates to randomClassmates while the array is not full enough,
    do {
        //get a number from 0 to 41
        const randomNum = randomNumber(classmatesList); 
        
        //if randomClassmates dosen't have that random classmate, add it to randomClassmates
        if(!(randomClassmates.includes(classmatesList[randomNum]))) {
            randomClassmates.push(classmatesList[randomNum]); //adding a unique random classmate to the array
        }
    } while(randomClassmates.length <= amount);

    return randomClassmates;
};
 


//Function for generating 4 answer options. Function gets as a parameter one classmate (host)
const getAnswerOptions = classmate => {
    
    let answerOptions = []; //array for storing 4 answer options (classmates) NOT shuffled
    let answerOptionsShuffled = []; //array for storing 4 shuffled answer options (classmates), this array returns from function
    
    //Take all classmates(41) and keep only those whose gender is the same as the host classmate has
    const filterByGender = allClassmates.filter(mate => mate.gender === classmate.gender);

    answerOptions.push(classmate.name); //adding the host classmate's name to the answerOptions direct
        
    //while array with answerOptions is not full enough, keep adding answer option
    while(answerOptions.length < 4) {
        //get ONE random classmate from array of classmates with the same gender
        const randomClassmate = getRandomClassmates(filterByGender, 1);

        //if this classmate is not in the answerOptions array, then add it to the array
        if(!answerOptions.includes(randomClassmate[0].name)) {
            answerOptions.push(randomClassmate[0].name);
        }
    }

    //shuffling the answer options
    //while array answerOptionsShuffled is not full enough, keep adding elements to it
    do {
        //get a number from 0 to 3
        const rundomNum = randomNumber(answerOptions);

        //get one random classmate from answerOptions and check if it is not in the answerOptionsShuffle array, 
        //then add that classmate to answerOptionsShuffled
        if(!answerOptionsShuffled.includes(answerOptions[rundomNum])) {
            answerOptionsShuffled.push(answerOptions[rundomNum]);
        }
    } while(answerOptionsShuffled.length < answerOptions.length);
    
    return answerOptionsShuffled;
};

//function for generating HTML and right answers
//functiong gets as a parameter array with objects ({name: "name", photo: "images/name-surname.jpg", gender: "F"})
const generateHtmlAndAnswers = randomClassmatesList => {
    
    //array with 15 objects in it. Contains right answers for 15 question [{question: "question1", answer: "name surname"}, {}, ... ]
    let allRightAnswers = []; //this array returns from function, as well as let html

    let html = ""; //in the end it is one long string with HTML for 15 blocks of questions, returns from function
    

    //take array with 15 classmates and for every one generate HTML, as well as answer options
    randomClassmatesList.forEach((classmate, index) => {
        
        //Invoking the function for generating answer options, including the right answer
        const answerOptions = getAnswerOptions(classmate);

        //keeping the right answer
        allRightAnswers.push({question: `question${index+1}`, answer: classmate.name});

        //add generated HTML with answer options to variabel let html
        html += `
            <div class="question">
            
                <div class='question-img-box'>
                    <img class='question-img' src="${classmate.photo}" alt="Photo of a classmate" class="question-img">
                </div>
                <div class="answer-options">
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="question${index+1}" id="qu${index+1}-op1" value="${answerOptions[0]}">
                        <label class="form-check-label" for="qu${index+1}-op1">
                            ${answerOptions[0]}
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="question${index+1}" id="qu${index+1}-op2" value="${answerOptions[1]}">
                        <label class="form-check-label" for="qu${index+1}-op2">
                            ${answerOptions[1]}
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="question${index+1}" id="qu${index+1}-op3" value="${answerOptions[2]}">
                        <label class="form-check-label" for="qu${index+1}-op3">
                            ${answerOptions[2]}
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="question${index+1}" id="qu${index+1}-op4" value="${answerOptions[3]}">
                        <label class="form-check-label" for="qu${index+1}-op4">
                            ${answerOptions[3]}
                        </label>
                    </div>
                </div>
            </div>
        `;
    });
    return {html: html, allRightAnswers: allRightAnswers};
};

//function for evaluating users answers
const evaluateAnswers = (userAnswers, rightAnswers) => { //rightAnswers is an array with objects [{question: "question1", answer: "name surname"}, {} ...]
    
    let scores = 0; //for counting how many answers was correct, result returns from function
    
    

    for(let i = 0; i < userAnswers.length; i++) { //userAnswers is an array with strings ["name surname", ..]
        
        //take a name from right answers array, make all letters small 
        //and add between the name and its surname "-", so it looks like a part of an image source
        const imgSrcFromName = rightAnswers[i].answer.toLowerCase().replace(' ', '-').replace(' ', '-'); //name-surname-additionalname
        
        let quEl; //here will be a div "Question" whose descendant includes image-element with right source name

        const evalDivEl = document.createElement('div');//create an empty div
        
        evalDivEl.classList.add('msg'); //adding class to the newly created empty div 
        
        //take all img elements on the page 
        document.querySelectorAll('img').forEach(img => {
            // find among all img elements one element which source inkludes right name
            if(img.src.includes(`${imgSrcFromName}`)) {
                quEl = img.parentElement.parentElement; //find right div "Question" whose descendant includes image-element with that source name
                
                if(quEl.querySelector('.msg')) {//if this div "Question" already has message from previous time, then delete it
                    quEl.querySelector('.msg').remove();
                }
                quEl.prepend(evalDivEl); //adding the empty div (with class 'msg') to the div "Question"
            }
        });
        
        //adding text to the empty div 'msg' depending on answer
        if(userAnswers[i] === rightAnswers[i].answer) {
            //if answer was correct, add this text
            evalDivEl.innerText = `✔️ Correct! That is ${rightAnswers[i].answer}!`;
            //if answer was correct, add also one score 
            scores++;
            
        } else {
            //if answer was NOT correct, add this text
            evalDivEl.innerText = `❌ Wrong! That is ${rightAnswers[i].answer}!`;
        }
    }
    return scores;
};

// variable to store the best score
let bestScore = 0;

//function for comparing the best score with current score
//best score is stored in the global scope
const bestScoreControl = scores => {
    if (bestScore > 0) { 
        bestScore > scores ? bestScore = bestScore : bestScore = scores;
    } else {
        bestScore = scores;
    }
};

//function for generating and inserting div-block with current and best scores at the end of the game
const generateScoresBlock = scores => {
    
    //text in the heading in the score's block will depend on how many right answers the user has
    let h3El;

    if (scores === 15) {
        h3El = `<h3 class="mt-3 text-success scores-heading">Wow! ${scores} scores out of 15! You've really mastered your classmates' names! 👏</h3>`;
    } else if(scores < 5 && scores < bestScore && bestScore > 0) {
        h3El = `<h3 class="mt-3 text-danger scores-heading">Only ${scores} scores out of 15. You can better!</h3>`;
    } else {
        h3El = `<h3 class="mt-3 scores-heading">You answered right on ${scores} questions out of 15!</h3>`;
    }

    //generating html
    let html = `
        ${h3El}
        <div class="scores">
            <p class="scores-best">Best score: ${bestScore}</p> 
            <p class="scores-current">Current score: ${scores}</p>
            <button class="btn-try-again">Try <br> again</button>
        </div>`;

    //inserting html
    document.querySelector('.scores-block').innerHTML = html;
};

//function for starting the game
const startGame = () => {
    //Invoking the function for generating random classmates
    const randomClassmates = getRandomClassmates(allClassmates, 15); //array with 15 objects
    
    //Invoking the function for generating Html and right answers. The variable stors 2 elements ({html: string, allRightAnswers: array})
    const htmlAndAnswers = generateHtmlAndAnswers(randomClassmates);
    
    //inserting HTML on the page
    document.querySelector('.questions-box').innerHTML = htmlAndAnswers.html;
    
    //getting the element where EventListener will be added
    const quizFormEl = document.querySelector('.quiz-form');
    
    //adding event listener to the whole form (".quiz-form")
    quizFormEl.addEventListener('submit', e => {
        e.preventDefault(); //the page will not reload
    
        //get user's answers from all questions
        const userAnswers = [
            quizFormEl.question1.value,
            quizFormEl.question2.value,
            quizFormEl.question3.value,
            quizFormEl.question4.value,
            quizFormEl.question5.value,
            quizFormEl.question6.value,
            quizFormEl.question7.value,
            quizFormEl.question8.value,
            quizFormEl.question9.value,
            quizFormEl.question10.value,
            quizFormEl.question11.value,
            quizFormEl.question12.value,
            quizFormEl.question13.value,
            quizFormEl.question14.value,
            quizFormEl.question15.value
        ];
    
        //scroll to the top of the page
        scrollTo(0,0);
    
        //Invoking function for evaluating user's answers. The variable stores amount of the right answers
        const scores = evaluateAnswers(userAnswers, htmlAndAnswers.allRightAnswers);

        //Invoking the function for comparing the best score with current score
        bestScoreControl(scores);

        //Invoking function for generating and inserting on the page div-block with current and best scores
        generateScoresBlock(scores);

        //getting the element where EventListener will be added (button try again)
        const btnTryAgainEl = document.querySelector('.btn-try-again');
        
        //adding event listener to the button "try again"
        btnTryAgainEl.addEventListener('click', () => {
            
            //change heading text 
            document.querySelector('.scores-heading').innerHTML = `<h3 class="mt-3 scores-heading">Choose the right answer</h3>`;

            //start the game again
            startGame();
        });
    });
};

//Invoking function for starting the game when the page is loaded
startGame();



